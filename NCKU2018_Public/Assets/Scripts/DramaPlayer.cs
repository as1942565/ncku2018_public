﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DramaPlayer : MonoBehaviour 
{
	#region Singleton
	public static DramaPlayer self;
	/*private Transform LoadingScene;
	private GameObject PlayerDataManager;
	private GameObject Level1AudPlayer;	//BGM播放器*/

	private DramaPlayer()
	{

	}

	public void Awake ()
	{
		self = this;

		/*PlayerDataManager = GameObject.Find ( "/PlayerDataManager" );
		if ( PlayerDataManager == null )
			SceneManager.LoadScene ( "GameStart2" );
		
		Level1AudPlayer = GameObject.Find ( "Level_AudPlayer" );
		if ( Level1AudPlayer != null )
			Level1AudPlayer.GetComponent<AudioSource> ().Play ();*/
	}
	#endregion

	#region Canvas
	[SerializeField]                  		 		//為了讓外面可以看到
	private Canvas backgroundCanvas = null;			//背景層 (後)
	//private : 不讓人編輯
	[SerializeField]
	private Canvas objectCanvas = null;				//場景道具層 (後中)
	[SerializeField]
	private Canvas actorCanvas = null;				//人物道具層 (中)
	[SerializeField]
	private Canvas midgroundCanvas = null;			//插圖&選擇層 (中前)
	[SerializeField]
	private Canvas foregroundCanvas = null;			//文字&特效層 (前)

	public Canvas BackgroundCanvas
	{
		get {  return backgroundCanvas; }
	}
	public Canvas ObjectCanvas
	{
		get {  return objectCanvas; }
	}
	public Canvas ActorCanvas
	{
		get {  return actorCanvas; }
	}
	public Canvas MidgroundCanvas
	{
		get {  return midgroundCanvas; }
	}
	public Canvas ForegroundCanvas
	{
		get {  return foregroundCanvas; }
	}

	#endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;		//使用DoTween

public class Picture : MonoBehaviour 
{
	public GameObject DefaultImage = null;		//當Prefab使用

	private List<Image> Pictures = new List<Image>();

	public static Picture self;
	private Picture ()	//建構式
	{
	}
	void Awake ()
	{
		self = this;
		if ( DefaultImage == null )								//如果Prefab沒東西
			Debug.LogError ( "DefaultImage is null" );			//回報錯誤
	}

	public void ChangePicture ( Sprite sprite , float duration )		//變換插圖 ( 插圖 , 淡入淡出時間 )
	{
		Image picture = Instantiate ( DefaultImage , transform ).GetComponent<Image>();
		picture.sprite = sprite;
		picture.color = new Color ( 1 , 1 , 1 , 0 );
		picture.DOFade ( 1 , duration );						//淡入變更的插圖
		Pictures.Add ( picture );
	}

	public void DeletePicture ( float duration )
	{
		for ( int i = 0; i < Pictures.Count; i++ )
		{
			Pictures [i].DOFade ( 0 , duration );
			Destroy ( Pictures[i] , duration );
		}
		Pictures.Clear ();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;		//使用DoTween

public class Background : MonoBehaviour 
{
	[SerializeField]
	private Image image1 = null;		//現在的圖
	[SerializeField]
	private Image image2 = null;		//要變換的圖
	private Image Using;				//現在的圖得暫存區
	private Image alt;					//要變換的圖的暫存區

	public static Background self;
	private Background ()	//建構式
	{
	}
	void Awake ()
	{
		self = this;
		if ( image1 == null || image2 == null )				//如果image1跟2其中一個是空值
			Debug.LogError ( "image1 or 2 is null" );		//回報錯誤
		Using = image1;										//把image1設定給Using
		Using.color = new Color ( 1 , 1 , 1 , 0 );			//把圖變成透明
		alt = image2;										//把image2設定給alt
		alt.color = new Color ( 1 , 1 , 1 , 0 );			//把圖變成透明
	}

	public void ChangeBackground ( Sprite sprite , float duration )		//變換背景圖 ( 圖片 , 淡入淡出時間 )
	{
		alt.sprite = sprite;								//設定要變更的圖
		alt.DOFade ( 1 , duration );						//淡入變更的圖
		Using.DOFade ( 0 , duration );						//淡出原本的圖
		ToggleImage ();										//圖片交接
	}

	private void ToggleImage ()
	{
		Image i = alt;		//變更的圖放入暫存
		alt = Using;		//現在的圖設定給變更的圖
		Using = i;			//暫存的圖給現在的圖
	}
}

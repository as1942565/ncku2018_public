﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour 
{
	public static PlayerData instance;
	void Awake ()
	{
		instance = this;
	}

	public int Level;			//關卡數
	public bool[] ClearLevel = new bool[9];
	public bool[] bag = new bool[6];
	public bool[] GetItem = new bool[6];
	public bool[] UseItem = new bool[6];
	public bool[] Condition = new bool[5];

	public List<GameObject> Map = new List<GameObject>();			//地圖物件
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UGUIData : MonoBehaviour
{
	public static UGUIData self;
	private UGUIData ()
	{
	}
	void Awake ()
	{
		self = this;
	}

	public Transform ActorCanvas;		//角色圖層
	public Transform ForegroundCanvas;	//UI圖層
	public Transform ObjectCanvas;		//可點擊物件圖層
	public Transform MapAnimation;		//地圖

	public GameObject FSMOwner;			//FSM播放器

	public Text ActorName;				//角色名字
	public Text ActorText;				//角色對話

	public Image LoadingScene;			//過場物件

	public GameObject[] MainButtonMask;	//主要按鈕遮罩
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BagData : MonoBehaviour 
{
	public static BagData self;
	public Transform ItemBox;			//道具紅框
	void Awake ()
	{
		self = this;
	}

	public void GetItem ( int item )
	{
		for ( int i = 0; i < PlayerData.instance.bag.Length; i++ )
		{
			if ( PlayerData.instance.bag[i] == false )
			{
				PlayerData.instance.bag [i] = true;
				//Todo : 去把這個東西生在背包裡面
				break;
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
public class BGMPlayer : MonoBehaviour 
{
    private static BGMPlayer _instance;
	private const string ResPath = "Prefab/BGMPlayer";		//BGMPlayer在Resources資料夾的路徑
    public static BGMPlayer instance 
	{
        get 
		{			
			if ( _instance == null )		//如果場景中沒有BGMPlayer
			{		
				_instance = FindObjectOfType<BGMPlayer> ();
				if ( _instance == null )
				{
					_instance = GameObject.Instantiate ( Resources.Load<BGMPlayer> ( ResPath ) );
					//就生產一個
					//Instantiate : 生產
					//Resources.Load 從Resources資料夾中讀取出來 (Resources是固定的 不能打錯)
					_instance.init();		//初始化
				}
			}
			return _instance;			//並且回傳給他
        }
    }
	//建構式 : 意義上來說 是希望不要讓其他人有辦法去Creat這個Object 因為是一個常態存在 理會上只會有一個
	private BGMPlayer()				//防呆用 沒有這行的話 預設是public 改成private後別如就沒有存取權限
	{    
	}
	private bool isInit = false;	//是否已經初始化
	private void init ()			//初始化的function
	{
        if ( isInit )
            return;

        _instance = this;
		DontDestroyOnLoad( this );						//DontDestroyOnLoad : 換場景時不要刪除
		if ( audioSource == null ) 						//如果找不到audioSource
		{
			audioSource = GetComponent<AudioSource>();	//就指定自己Component中的AudioSource給他
        }
		isInit = true;				//已經初始化

    }
    public void Awake() 
	{
        init();
    }

	[SerializeField]				//為了可以在Unity中被看到和編輯 (但不可被Script中呼叫)
	private AudioSource audioSource;					//播放音樂用

	private AudioClip Tempclip = null;
	public void PlayBGM ( AudioClip clip , bool FadeInOut = false ) 		//切換音樂並播放
	{
		if ( clip != Tempclip )
			Tempclip = clip;
		else
			return;
		if ( FadeInOut ) 		//如果需要淡出
		{
			FadeOut( 2f, () => 							//音樂淡出
			{	
				//() => : 一個沒有名字的虛擬function 並且將以下這些事情存到TweenCallback內
				audioSource.clip = clip;				//切換音樂
                audioSource.time = 0;					//從頭開始播
				audioSource.Play();						//播放
				FadeIn();								//音樂淡入
            } );

        } 
		else 					//否則
		{
			audioSource.clip = clip;					//切換音樂
			audioSource.time = 0;						//從頭開始播
			audioSource.Play();							//播放
        }
    }

	public void Pause() 								//暫停
	{
		audioSource.Pause();							//暫停
    }

	public void FadeBGM( float volume , float duration , TweenCallback onComplete = null ) 	//改變音量 ( 音量 , 花多久時間 , 改變完音量之後要呼叫function )
	{
		Tweener t = audioSource.DOFade ( volume, duration );	//開始改變音量 並且回傳一個新的Tweener
		t.onComplete += onComplete;								//將改變完音量之後的事情傳給Tweener
    }

	public void FadeOut ( float duration = 2f , TweenCallback onComplete =null ) 	//淡出音量 ( 音量 , 改變完音量之後要呼叫function )
	{
		FadeBGM ( 0 , duration , onComplete );
    }
	public void FadeIn ( float duration = 2f , TweenCallback onComplete = null ) 	//淡入音量 ( 音量 , 改變完音量之後要呼叫function )
	{
		FadeBGM ( 1 , duration , onComplete );
    }

	public void SetVolume ( float volume ) 				//設定音量
	{
		audioSource.volume = volume;					//設定音量
    }		

}

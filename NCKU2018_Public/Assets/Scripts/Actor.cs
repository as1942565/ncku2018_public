﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.DialogueTrees;		

public class Actor : DialogueActor
{
    private Emotion activeSprite;	
    public SpriteRenderer spriterenderer;
    public Texture2D[] textures = new Texture2D[2];	

    public void Awake() 
	{
		if( spriterenderer == null ) 		//如果spriterenderer是空的
		{
			spriterenderer = GetComponent<SpriteRenderer>();	//指定SpriteRenderer給他
        }
    }

    public void SetEmotion(Emotion emo)		
	{
        _portrait = textures[(int)emo];
    }

    public enum Emotion 				
	{
        idle =0,
        happy =1,
        mad =2 ,
        sad =3,
        shock =4,
    }
}


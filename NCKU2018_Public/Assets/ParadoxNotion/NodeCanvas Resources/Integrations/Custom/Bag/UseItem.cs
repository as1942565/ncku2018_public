﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.Framework;		//寫事件時比須要有
using ParadoxNotion.Design;		//寫事件時比須要有

namespace NodeCanvas.Tasks.Actions 
{
	//namespace : 類似資料夾 (方便管理)
	//NodeCanvas.Tasks.Actions : 資料夾名稱
	//須要呼叫此腳本時 需加入 using.NodeCanvas.Tasks.Actions;
	[Category("Bag")]			//在Task Action裡的Assign Action Task中的一個資料夾 
	public class UseItem : ActionTask 
	{
		public int ItemNum;

		protected override void OnExecute() 		//運行 (只執行一次)
		{
			if ( PlayerData.instance.bag[ItemNum] )
			{
				//PlayerData.instance.UseItem [ItemNum] = !ShowBox;
				if ( PlayerData.instance.UseItem[ItemNum] )
				{
					BagData.self.ItemBox.gameObject.SetActive ( false );
					PlayerData.instance.UseItem[ItemNum] = false;
				}
				else
				{
					BagData.self.ItemBox.gameObject.SetActive ( true );
					for ( int i = 0; i < PlayerData.instance.UseItem.Length; i++ )
						PlayerData.instance.UseItem[i] = i == ItemNum;
				}

				BagData.self.ItemBox.position = agent.transform.position;
			}

			EndAction ();				//結束執行 (放在這邊就不會執行Update 這行也可以放在Update)
		}
	}
}

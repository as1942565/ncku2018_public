﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.Framework;		//寫事件時比須要有
using ParadoxNotion.Design;		//寫事件時比須要有

namespace NodeCanvas.Tasks.Actions 
{
	//namespace : 類似資料夾 (方便管理)
	//NodeCanvas.Tasks.Actions : 資料夾名稱
	//須要呼叫此腳本時 需加入 using.NodeCanvas.Tasks.Actions;
	[Category("Bag")]			//在Task Action裡的Assign Action Task中的一個資料夾 
	public class GetItem : ActionTask 
	{
		public int ItemNum;
		public BBParameter<Sprite> ItemSpr;

		private string ItemPath = "Prefab/Bag/Item";
		private string ObjectPath = "Prefab/Bag/Object";
		private GameObject Object;

		protected override void OnExecute() 		//運行 (只執行一次)
		{
			BagData.self.GetItem ( ItemNum );
			PlayerData.instance.GetItem [ItemNum - 1] = true;
			Transform UI = DramaPlayer.self.ForegroundCanvas.transform.Find ( "UI" );
			Object = GameObject.Instantiate ( Resources.Load ( ObjectPath ) , UI ) as GameObject;
			Object.transform.GetChild( 0 ).GetComponent<Image>().sprite = ItemSpr.value;

			Transform[] bags = DramaPlayer.self.ForegroundCanvas.transform.Find ( "UI/Bag" ).GetComponentsInChildren<Transform> ();
			for ( int i = 1; i < bags.Length; i++ )
			{
				if ( bags[i].name == "Image" + ( ItemNum - 1 ).ToString() )
				{
					GameObject item = GameObject.Instantiate ( Resources.Load ( ItemPath ) , bags[i] ) as GameObject;
					item.GetComponent<Image> ().sprite = ItemSpr.value;
					item.transform.localPosition = Vector3.zero;
					break;
				}
			}
		}
		protected override void OnUpdate ()			//運行 (執行無限次)
		{
			if ( Object != null )
			{
				if ( Input.GetMouseButtonDown ( 0 ) )
					GameObject.Destroy ( Object );
			}
			else
				EndAction ();				//結束執行 (放在這邊就不會執行Update 這行也可以放在Update)
		}
	}
}

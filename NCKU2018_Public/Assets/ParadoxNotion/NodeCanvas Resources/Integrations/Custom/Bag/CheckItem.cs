﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.Framework;		//寫事件時比須要有
using ParadoxNotion.Design;		//寫事件時比須要有

namespace NodeCanvas.Tasks.Actions 
{
	//namespace : 類似資料夾 (方便管理)
	//NodeCanvas.Tasks.Actions : 資料夾名稱
	//須要呼叫此腳本時 需加入 using.NodeCanvas.Tasks.Actions;
	[Category("Bag")]			//在Task Action裡的Assign Action Task中的一個資料夾 
	public class CheckItem : ConditionTask 
	{
		[BlackboardOnly]
		public bool valueB;
		public int index;

		protected override string info
		{
			get 
			{
				return "UseItem" + "[" + index + "] == " + valueB.ToString();
			}
		}

		protected override bool OnCheck()
		{
			return PlayerData.instance.UseItem [index] == valueB;
		}
	}
}

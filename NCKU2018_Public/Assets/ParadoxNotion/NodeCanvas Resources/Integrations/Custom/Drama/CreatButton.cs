﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.Framework;			//寫事件時比須要有
using ParadoxNotion.Design;			//寫事件時比須要有

namespace NodeCanvas.Tasks.Actions
{
	//namespace : 類似資料夾 (方便管理)
	//NodeCanvas.Tasks.Actions : 資料夾名稱
	//須要呼叫此腳本時 需加入 using.NodeCanvas.Tasks.Actions;

	[Category("Drama")]				//在Task Action裡的Assign Action Task中的一個資料夾
	//"Drama" : 資料夾名稱

	public class CreatButton : ActionTask 
	{
		public GameObject ButtonClick;					//可點擊的Button
		public List<Vector3> Position;					//Button的位置
		public List<Vector2> Size;						//Button的大小
		protected override void OnExecute ()
		{
			base.OnExecute ();
			for ( int i = 0; i < Position.Count; i++ )
			{
				GameObject button = ( GameObject )GameObject.Instantiate ( ButtonClick , UGUIData.self.ObjectCanvas );		//生產Button
				button.GetComponent<Blackboard>().SetValue( "Number" , i );							//給予編號
				button.GetComponent<RectTransform> ().transform.localPosition = Position [i];		//給予位置
				button.GetComponent<RectTransform> ().sizeDelta = Size [i];							//給予大小
			}
			EndAction ();							//結束執行 (放在這邊就不會執行Update 這行也可以放在Update)
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.Framework;				//寫事件時比須要有
using ParadoxNotion.Design;				//寫事件時比須要有

namespace NodeCanvas.Tasks.Actions
{
	//namespace : 類似資料夾 (方便管理)
	//NodeCanvas.Tasks.Actions : 資料夾名稱
	//須要呼叫此腳本時 需加入 using.NodeCanvas.Tasks.Actions;
	[ Category ( "Drama" ) ]			//在Task Action裡的Assign Action Task中的一個資料夾
	//"Drama" : 資料夾名稱
	public class DeletePicture : ActionTask
	{
		public BBParameter<float> duration;		//淡入淡出時間

		protected override void OnExecute ()
		{
			Picture.self.DeletePicture ( duration.value );				//淡出需要幾秒
			EndAction ();												//結束執行 (放在這邊就不會執行Update 這行也可以放在Update)
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.Framework;		//寫事件時比須要有
using ParadoxNotion.Design;		//寫事件時比須要有

namespace NodeCanvas.Tasks.Actions 
{
	//namespace : 類似資料夾 (方便管理)
	//NodeCanvas.Tasks.Actions : 資料夾名稱
	//須要呼叫此腳本時 需加入 using.NodeCanvas.Tasks.Actions;

	[Category("Drama")]			//在Task Action裡的Assign Action Task中的一個資料夾
	//"Drama" : 資料夾名稱

	public class SetTextDisplay : ActionTask 
	{
		protected override void OnExecute ()
		{
			base.OnExecute ();
			UGUIData.self.ActorName.text = "";		//隱藏角色名字
			UGUIData.self.ActorText.text = "";		//隱藏角色對話
			EndAction();
		}
	}
}

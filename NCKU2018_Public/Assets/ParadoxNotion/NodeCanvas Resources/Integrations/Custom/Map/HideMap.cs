﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.Framework;		//寫事件時比須要有
using ParadoxNotion.Design;		//寫事件時比須要有

namespace NodeCanvas.Tasks.Actions 
{
	//namespace : 類似資料夾 (方便管理)
	//NodeCanvas.Tasks.Actions : 資料夾名稱
	//須要呼叫此腳本時 需加入 using.NodeCanvas.Tasks.Actions;

	[Category("Map")]			//在Task Action裡的Assign Action Task中的一個資料夾
	//"Map" : 資料夾名稱

	public class HideMap : ActionTask 
	{
		public string HideNum;			//要隱藏的地圖編號

		protected override void OnExecute ()
		{
			base.OnExecute ();

			Sprite UnlockMap = Resources.Load<Sprite> ( "Sprite/Map/UnlockBtn" );
			string[] hide = HideNum.Split ( '-' );
			if ( PlayerData.instance.Map.Count != 0 )
			{
				for ( int i = 0; i < hide.Length; i++ )
				{
					if ( int.Parse ( hide[i] ) - 1 >= PlayerData.instance.Map.Count )
						continue;
					PlayerData.instance.Map[ int.Parse ( hide[i] ) - 1 ].GetComponent<Image>().sprite = UnlockMap;
					PlayerData.instance.Map[ int.Parse ( hide[i] ) - 1 ].tag = "Untagged";
					PlayerData.instance.Map[ int.Parse ( hide[i] ) - 1 ].GetComponentInChildren<Text>().text = "？？？";
				}
			}

			EndAction ();						//結束執行 (放在這邊就不會執行Update 這行也可以放在Update)
		}
	}
}

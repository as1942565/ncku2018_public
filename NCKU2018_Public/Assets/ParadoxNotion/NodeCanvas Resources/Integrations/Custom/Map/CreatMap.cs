﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.Framework;		//寫事件時比須要有
using ParadoxNotion.Design;		//寫事件時比須要有
using NodeCanvas.StateMachines;

namespace NodeCanvas.Tasks.Actions 
{
	//namespace : 類似資料夾 (方便管理)
	//NodeCanvas.Tasks.Actions : 資料夾名稱
	//須要呼叫此腳本時 需加入 using.NodeCanvas.Tasks.Actions;

	[Category("Map")]			//在Task Action裡的Assign Action Task中的一個資料夾
	//"Map" : 資料夾名稱

	public class CreatMap : ActionTask 
	{
		public List<string> SceneName;				//場景名稱
		public List<FSM> ScenePlayer;

		private int[] PX = new int[]{ -190 ,  0 , 190 , -190 ,    0 ,  190 };		//地圖X位置
		private int[] PY = new int[]{   70 , 70 ,  70 , -100 , -100 , -100 };		//地圖Y位置
		private string MapPath = "Prefab/Map/Map";
		private Sprite Move;					//可移動地區
		private Sprite Now;						//現在地區

		protected override void OnExecute ()
		{
			base.OnExecute ();

			Transform Map = UGUIData.self.ForegroundCanvas.Find ( "UI/MapAnimation" );

			//移除舊的地圖
			Transform[] Maps = UGUIData.self.ForegroundCanvas.Find ( "UI/MapAnimation" ).GetComponentsInChildren<Transform>();
			for ( int i = 0; i < Maps.Length; i++ )
			{
				if ( Maps[i].tag == "Map" )
					GameObject.Destroy ( Maps[i].gameObject );
			}
			//清除陣列位置
			PlayerData.instance.Map.Clear();

			//創立新的地圖
			for ( int i = 0; i < SceneName.Count; i++ )						//生產數個地圖
			{
				GameObject map;
				int l = PlayerData.instance.Level % 10;						//取得第幾個場景
				Move = Resources.Load<Sprite> ( "Sprite/Map/MoveBtn" );	//讀取可移動地區圖案
				Now = Resources.Load<Sprite> ( "Sprite/Map/Now" );		//讀取現在地區圖案
				map = ( GameObject )GameObject.Instantiate ( Resources.Load ( MapPath ) , Map );
				map.name = ( i + 1 ).ToString ();
				map.GetComponent<Image>().sprite = ( l == i + 1 ) ? Now : Move;
				map.transform.localPosition = new Vector2 ( PX[i] , PY[i] );		//變更位置
				map.GetComponent<Blackboard>().SetValue( "myFSM" , ScenePlayer[i] );

				PlayerData.instance.Map.Add ( map );			//加入地圖到玩家資料
			}

			//更改地圖文字
			for ( int i = 0; i < PlayerData.instance.Map.Count; i++ )
				PlayerData.instance.Map[i].transform.Find( "Text" ).GetComponent<Text>().text = SceneName[i];
			
			EndAction ();						//結束執行 (放在這邊就不會執行Update 這行也可以放在Update)
		}
	}
}

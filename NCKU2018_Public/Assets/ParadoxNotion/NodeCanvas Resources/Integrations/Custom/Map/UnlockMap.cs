﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.Framework;		//寫事件時比須要有
using ParadoxNotion.Design;		//寫事件時比須要有
using NodeCanvas.StateMachines;

namespace NodeCanvas.Tasks.Actions 
{
	//namespace : 類似資料夾 (方便管理)
	//NodeCanvas.Tasks.Actions : 資料夾名稱
	//須要呼叫此腳本時 需加入 using.NodeCanvas.Tasks.Actions;

	[Category("Map")]			//在Task Action裡的Assign Action Task中的一個資料夾
	//"Map" : 資料夾名稱

	public class UnlockMap : ActionTask 
	{
		public List<int> SceneNum;						//地圖編號
		public List<string> SceneName;					//地圖名稱
		//public bool ShowMap;							//是否顯示地圖

		protected override void OnExecute ()
		{
			base.OnExecute ();

			Sprite MoveMap = Resources.Load<Sprite> ( "Sprite/Map/MoveBtn" );		//找到地圖的圖片

			for ( int i = 0; i < SceneNum.Count; i++ )
			{
				PlayerData.instance.Map [ SceneNum[i] - 1 ].GetComponent<Image>().sprite = MoveMap;		
				//更改圖片
				PlayerData.instance.Map [ SceneNum[i] - 1 ].tag = "Map";
				//更改標籤
				PlayerData.instance.Map [ SceneNum[i] - 1 ].GetComponentInChildren<Text>().text = SceneName[i];
				//更改地圖名稱
			}

			//if ( ShowMap )
			//	DramaPlayer.self.ForegroundCanvas.transform.Find ( "UI" ).GetComponent<ButtonCtl> ().ShowMap ();
			
			EndAction ();						//結束執行 (放在這邊就不會執行Update 這行也可以放在Update)
		}
	}
}


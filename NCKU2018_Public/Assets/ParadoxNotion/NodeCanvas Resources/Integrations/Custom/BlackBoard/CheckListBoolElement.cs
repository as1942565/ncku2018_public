﻿using NodeCanvas.Framework;
using ParadoxNotion.Design;
using System.Collections.Generic;
using System;

namespace NodeCanvas.Tasks.Conditions
{
	[Category("✫ Blackboard")]
	public class CheckListBoolElement : ConditionTask
	{
		[BlackboardOnly]
		public BBParameter<List<bool>> valueA;
		public BBParameter<bool> valueB;
		public int index;

		protected override string info
		{
			get 
			{
				return valueA + "[" + index + "] == " + valueB;
			}
		}

		protected override bool OnCheck()
		{
			return valueA.value[index] == valueB.value;
		}
	}
}
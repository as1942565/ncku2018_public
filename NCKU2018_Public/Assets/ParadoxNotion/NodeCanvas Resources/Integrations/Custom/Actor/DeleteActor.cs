﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;		//寫事件時比須要有
using ParadoxNotion.Design;		//寫事件時比須要有


namespace NodeCanvas.Tasks.Actions 
{
//namespace : 類似資料夾 (方便管理)
//NodeCanvas.Tasks.Actions : 資料夾名稱
//須要呼叫此腳本時 需加入 using.NodeCanvas.Tasks.Actions;

	[Category( "Actor" )]			//在Task Action裡的Assign Action Task中的一個資料夾
	//"Actor" : 資料夾名稱
    public class DeleteActor : ActionTask<Actor>
	{
		protected override void OnExecute() 			//運行 (只執行一次)
		{			
			Actor[] actor = UGUIData.self.ActorCanvas.GetComponentsInChildren<Actor> ();
			for ( int i = 0; i < actor.Length; i++ )
			{
				if ( actor[i].gameObject.name == agent.gameObject.name )

					GameObject.Destroy( actor[i].gameObject );				
			}
			EndAction();								//結束執行 (放在這邊就不會執行Update 這行也可以放在Update)
        }
    }
}
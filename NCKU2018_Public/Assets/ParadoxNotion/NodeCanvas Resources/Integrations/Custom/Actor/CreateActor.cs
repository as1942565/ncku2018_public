﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;		//寫事件時比須要有
using ParadoxNotion.Design;		//寫事件時比須要有

namespace NodeCanvas.Tasks.Actions 
{
//namespace : 類似資料夾 (方便管理)
//NodeCanvas.Tasks.Actions : 資料夾名稱
//須要呼叫此腳本時 需加入 using.NodeCanvas.Tasks.Actions;

	[Category("Actor")]			//在Task Action裡的Assign Action Task中的一個資料夾 
	//"Actor" : 資料夾名稱
	//[Icon("DOTTween", true)]	//小圖示(不重要)
	public class CreateActor : ActionTask<Actor> 	//<Actor> : 有可以使用自己的選項 原本是使用自己的ActionTask 現在是使用ActionTask中的Actor
	{  
		//public BBParameter<Actor> Actor;			//生成物件(角色)
		//BBParameter : 可以使用黑版的變數 (要開啟點點)
		//因為BBParameter的關係 Actor現在是一個變數 不是一個Actor
		//所以使用時要加入.value 將他變回Actor 需要特別注意
		//因為有16行的<Actor> 所以就不需要18行 因為已經可以選擇要指定自己或指定其他

		public float CustomPos;						//生成位置的X軸	

		public ActorPos position = ActorPos.left;	//宣告一個ActorPos 遇設為Left
		public enum ActorPos	//列舉 (可選擇的選項)
		{			
			left = -180,
			middle = -50,
			right = 180,
			custom = 0,
		}
		Vector3 GetPosition ( ActorPos pos )		//宣告一個Vector3 並且取得ActorPos的值
		{
			if ( pos == ActorPos.custom )			//如果並且取得ActorPos的值是custom
				return new Vector3 ( CustomPos , 20 , 0 );	//回傳( CustomPos , 20 , 0 ) 並跳出
			return new Vector3 ( ( int )pos , 20 , 0 );		//回傳( ActorPos選擇的 , 20 , 0 ) 並跳出
		}

		public bool UseGlobalCoordinate = false;	//是否要轉成Global

		protected override void OnExecute() 		//運行 (只執行一次)
		{
			Actor instance =  GameObject.Instantiate<Actor> ( agent , UGUIData.self.ActorCanvas );	
			instance.gameObject.name = agent.gameObject.name;				
			//生成自己(Actor) 並且放在特定物件上當子物件
			if ( UseGlobalCoordinate )				//如果要轉成Global
				instance.transform.position = GetPosition( position );		//Position的值給transform.position 
			else                                    //否則
				instance.transform.localPosition = GetPosition( position );	//Position的值給transform.localPosition 
			//this.blackboard.AddVariable(agent.name, instance);
			//this.blackboard.AddVariable ( agent.name , actor );
			//blackboard.AddVariable : 再黑板中加入變數 ( 名稱 , 類型 )
			//agent : <>裡面放什麼 就會是什麼 (這邊指的是15行的Actor)
			EndAction ();				//結束執行 (放在這邊就不會執行Update 這行也可以放在Update)

        }
		protected override void OnUpdate ()			//運行 (執行無限次)
		{
		}
    }

}

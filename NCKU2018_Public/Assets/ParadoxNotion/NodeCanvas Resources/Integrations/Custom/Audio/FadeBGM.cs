﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;		//寫事件時比須要有
using ParadoxNotion.Design;		//寫事件時比須要有

namespace NodeCanvas.Tasks.Actions 
{
//namespace : 類似資料夾 (方便管理)
//NodeCanvas.Tasks.Actions : 資料夾名稱
//須要呼叫此腳本時 需加入 using.NodeCanvas.Tasks.Actions;

	[Category( "Audio" )]			//在Task Action裡的Assign Action Task中的一個資料夾
	//"Audio" : 資料夾名稱
    public class FadeBGM : ActionTask 
	{
		public BBParameter<float> volume;			//初始音量 (0~1 還是1~0)
		//BBParameter : 可以使用黑版的變數 (要開啟點點)
		//因為BBParameter的關係 float現在是一個變數 不是一個float
		//所以使用時要加入.value 將他變回float 需要特別注意
		public BBParameter<float> duration = 2;		//淡入淡出時間
        public bool waitActionFinish;				//是否等到淡入淡出結束後才做下一件事

		protected override void OnExecute() 		//運行 (只執行一次)
		{
            BGMPlayer.instance.FadeBGM ( volume.value, duration.value );
            if ( waitActionFinish == false )
			{
				EndAction();						//結束執行 (放在這邊就不會執行Update 這行也可以放在Update)
            }
        }
		protected override void OnUpdate() 			//運行 (執行無限次)
		{
			base.OnUpdate ();						//呼叫繼承前的Update
			//elapsedTime : 每個節點從開始執行到現在的時間 (更換節點會歸0)
			if ( elapsedTime >= duration.value )	//如果已經超過時間				
				EndAction();						//結束執行
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;		//寫事件時比須要有
using ParadoxNotion.Design;		//寫事件時比須要有

namespace NodeCanvas.Tasks.Actions 
{
	//namespace : 類似資料夾 (方便管理)
	//NodeCanvas.Tasks.Actions : 資料夾名稱
	//須要呼叫此腳本時 需加入 using.NodeCanvas.Tasks.Actions;

	[Category("Audio")]			//在Task Action裡的Assign Action Task中的一個資料夾
	//"Audio" : 資料夾名稱
	public class StopEffect : ActionTask 
	{
		protected override void OnExecute ()		//運行 (只執行一次)
		{
			AudioSource[] audios = Transform.FindObjectsOfType<AudioSource> ();		//場景中尋找有AudioSource組件的物件
			for ( int i = 0; i < audios.Length; i++ )
			{
				if ( audios[i].name == "EffectPlayer" )
					audios[i].loop = false;											//將擁有AudioSource的物件的Loop改成false
			}
			EndAction ();							//結束執行 (放在這邊就不會執行Update 這行也可以放在Update)
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;		//寫事件時比須要有
using ParadoxNotion.Design;		//寫事件時比須要有

namespace NodeCanvas.Tasks.Actions 
{
//namespace : 類似資料夾 (方便管理)
//NodeCanvas.Tasks.Actions : 資料夾名稱
//須要呼叫此腳本時 需加入 using.NodeCanvas.Tasks.Actions;

	[Category("Audio")]			//在Task Action裡的Assign Action Task中的一個資料夾
	//"Audio" : 資料夾名稱
    public class ChangeBGM : ActionTask 
	{
		public BBParameter<AudioClip> clip;			//要播放的音樂
		//BBParameter : 可以使用黑版的變數 (要開啟點點)
		//因為BBParameter的關係 AudioClip現在是一個變數 不是一個AudioClip
		//所以使用時要加入.value 將他變回AudioClip 需要特別注意
		public BBParameter<bool> FadeInOut = true;	//是否要淡入淡出

		protected override void OnExecute ()		//運行 (只執行一次)
		{
			BGMPlayer.instance.PlayBGM ( clip.value , FadeInOut.value );	//切換BGM
			EndAction ();				//結束執行 (放在這邊就不會執行Update 這行也可以放在Update)
		}
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;		//寫事件時比須要有
using ParadoxNotion.Design;		//寫事件時比須要有

namespace NodeCanvas.Tasks.Actions 
{
	//namespace : 類似資料夾 (方便管理)
	//NodeCanvas.Tasks.Actions : 資料夾名稱
	//須要呼叫此腳本時 需加入 using.NodeCanvas.Tasks.Actions;

	[Category("Audio")]			//在Task Action裡的Assign Action Task中的一個資料夾
	//"Audio" : 資料夾名稱
	public class PlayEffect : ActionTask 
	{
		public BBParameter<AudioClip> clip;			//要播放的音樂
		//BBParameter : 可以使用黑版的變數 (要開啟點點)
		//因為BBParameter的關係 AudioClip現在是一個變數 不是一個AudioClip
		//所以使用時要加入.value 將他變回AudioClip 需要特別注意
		public float Sec;							//幾秒後刪除物件
		public bool Loop;							//音效是否要Loop

		private string EffectPlayerPath = "Prefab/EffectPlayer";

		protected override void OnExecute ()		//運行 (只執行一次)
		{
			GameObject play = ( GameObject )GameObject.Instantiate ( Resources.Load ( EffectPlayerPath ) ) as GameObject;
			play.name = "EffectPlayer";
			play.GetComponent<AudioSource> ().clip = clip.value;
			play.GetComponent<AudioSource> ().loop = Loop;
			play.GetComponent<AudioSource> ().Play();
			if ( !Loop )
				GameObject.Destroy ( play , Sec );
			EndAction ();				//結束執行 (放在這邊就不會執行Update 這行也可以放在Update)
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(Actor))]
public class ActorEditor : Editor {
    private Actor actor { get { return (target as Actor); } }

    public void OnEnable() {
        int enumLength = Enum.GetNames(typeof(Actor.Emotion)).Length;
        if (actor.textures == null || actor.textures.Length != enumLength) {
            actor.textures = new Texture2D[enumLength];
            EditorUtility.SetDirty(actor);
        }
    }

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        float oldLabelWidth = EditorGUIUtility.labelWidth;
        EditorGUIUtility.labelWidth = 210;

        EditorGUI.BeginChangeCheck();
        string[] emotions = Enum.GetNames(typeof(Actor.Emotion));
        for (int i = 0; i < emotions.Length; i++) {
            actor.textures[i] = (Texture2D)EditorGUILayout.ObjectField(emotions[i], actor.textures[i], typeof(Texture2D), false, null);
        }
        if (EditorGUI.EndChangeCheck())
            EditorUtility.SetDirty(actor);

        EditorGUIUtility.labelWidth = oldLabelWidth;
    }

}
